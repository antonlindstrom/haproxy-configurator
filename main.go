package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

type TemplateData struct {
	Routes   map[string]string
	Backends map[string][]*Server
}

type Server struct {
	App     string
	Version string
	Node    string
	Mode    string
	Address string
	Port    string
}

func ParseServer(fields []string, activeVersions map[string]string) *Server {
	app := fields[0]
	version := fields[1]
	status := fields[5]

	activeVersion, exists := activeVersions[app]
	if !exists {
		activeVersion = "v0"
	}

	mode := ""

	if version != activeVersion {
		mode = "disabled"
	} else if status != "passing" {
		mode = "backup"
	}

	return &Server{
		App:     app,
		Version: version,
		Mode:    mode,
		Node:    fields[2],
		Address: fields[3],
		Port:    fields[4],
	}
}

func ParseConsulTemplateOutput(file *os.File) (*TemplateData, error) {
	scanner := bufio.NewScanner(file)
	routes := make(map[string]string)      // hostname to backend
	backends := make(map[string][]*Server) // server definitions per backend
	activeVersions := make(map[string]string)

	// Scan for routes
	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Split(line, " ")
		rowType := fields[0]
		fields = fields[1:]

		switch rowType {
		case "route:":
			app := fields[0]
			version := fields[1]
			host := fields[2]

			routes[host] = app
			activeVersions[app] = version

			// Create a new empty server list
			if _, exists := backends[app]; !exists {
				backends[app] = []*Server{}
			}
		case "server:":
			server := ParseServer(fields, activeVersions)
			servers, exists := backends[server.App]
			if !exists {
				servers = []*Server{}
			}

			backends[server.App] = append(servers, server)
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return &TemplateData{routes, backends}, nil
}

func fail(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format+"\n", a)
	os.Exit(1)
}

func main() {
	if len(os.Args) != 4 {
		fail("usage: %s consul-template-output haproxy-template output-file\n", os.Args[0])
	}

	consulTemplateOutput := os.Args[1]
	templateFile := os.Args[2]
	outputFile := os.Args[3]

	file, err := os.Open(consulTemplateOutput)
	if err != nil {
		fail("Failed to open consul-template output: %v", err)
	}
	defer file.Close()

	data, err := ParseConsulTemplateOutput(file)
	if err != nil {
		fail("Failed to parse consul-template output: %v", err)
	}

	tmpl, err := template.ParseFiles(templateFile)
	if err != nil {
		fail("Failed to parse haproxy template: %v", err)
	}

	// Create a file in the same directory as outputFile
	dir := filepath.Dir(outputFile)
	tmpfile, err := ioutil.TempFile(dir, ".haproxy-templator")
	if err != nil {
		fail("Failed to create temp file: %v", err)
	}

	// Render template and save to temp file
	w := bufio.NewWriter(tmpfile)
	if err := tmpl.Execute(w, data); err != nil {
		fail("Failed to execute template: %v", err)
	}
	w.Flush()

	if err := tmpfile.Close(); err != nil {
		fail("Failed to close temp file: %v", err)
	}

	// Move the temp file to output file (atomic operation)
	if err := os.Rename(tmpfile.Name(), outputFile); err != nil {
		fail("Failed to create output file: %v", err)
	}
}
