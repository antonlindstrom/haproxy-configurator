# haproxy-configurator

Example usage:

```
$ go build .

$ consul-template -template \
  'intermediate.ctmpl:intermediate.out:./main intermediate.out haproxy.template haproxy.cfg && cat etc/haproxy/haproxy.cfg'
```